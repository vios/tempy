#!/usr/bin/env python3

import socket
import os
import sys
import time
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('target_host', help="Target server that saves the temperature values. Hostnames are supported.", type=str)
parser.add_argument('port', help="Port of the target server", type=int)
parser.add_argument('-i', '--interval', help="How often the temperature gets sent in seconds. The default value is 300.", type=int, default=300)
args = parser.parse_args()

con = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ip = socket.gethostbyname(args.target_host)
port = args.port
address = (ip, port)
tick = args.interval

print(f"Connecting to {ip} on port {port}...")
con.connect(address)
print(f"Starting with sending data every {tick} seconds...")
while True:
    with open('/sys/class/thermal/thermal_zone0/temp') as temp:
        con.sendall(temp.read().encode('utf-8'))
        time.sleep(tick)
print("Service stopped")
