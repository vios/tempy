# Tempy

KISS network temperature monitor written in Python 3.6+ for the ASUS Tinkerboard and every system that happens to have a sensor in `/sys/class/thermal/thermal_zone0/temp`. 

## Why?

This script exists to monitor the temperator of your Tinkerboard but also don't kill your SD card with unnesserary writes. Instead it will send the data to a remote server of your choice. The remote server will write every value 
that it receives together with time and date in a simple CSV file so you can do your analysis later.

temp.py is the Client that sends its temperature status to some remote point.

serv-tem.py is the Server that receives the data. It is hardcoded to only handle one client at the time.

Both scripts support the `--help` parameter and are simple enough that there is not much more to it.

## Example

First on your target server you run

    python3 serv-temp.py 44203

On your Board you run *(In this example the server hostname is bobthebuilder)*

    python3 temp.py bobthebuilder 44203

And that's it. You should receive the current temperature in °C every 5 minutes.