#!/usr/bin/env python3

# TODO: Seperate date from time
# TODO: Header for CSV file

import socket
import argparse
from time import time
from datetime import datetime
from pathlib import Path

parser = argparse.ArgumentParser()
parser.add_argument(
    'port', help="The port you want to receive the data from", type=int)
parser.add_argument(
    '-s', '--storage', help="Path where you want the records to be saved. The defaults to the current working directory", type=str, default='./')
parser.add_argument(
    '-b', '--bind', help="IP you want to bind this script to. Usually needed on real servers with multiple NICs. Defaults to 0.0.0.0", type=str, default='0.0.0.0')
args = parser.parse_args()

HOST = args.bind
PORT = args.port
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen(1)
    print(f"Listening on {HOST}:{PORT}")
    conn, addr = s.accept()
    with conn:
        print(f"Connection accepted {addr[0]},{addr[1]}")
        record_path = Path(
            args.storage + f'temps_record{int(time())}.csv').resolve()
        with open(record_path, 'w') as csv:
            print(f"Started recording in {record_path}")
            while True:
                data = conn.recv(1024)
                if not data:
                    break
                rem = int(data.decode('utf8')) / 1000

                print(f"> Received data: {rem}°C")
                csv.write(f"{str(datetime.now())};{str(rem)}\n")
print("Exit")
